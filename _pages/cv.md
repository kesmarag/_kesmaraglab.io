---
layout: archive
# title: "CV"
permalink: /
author_profile: true
redirect_from:
  - /resume
---

{% include base_path %}

### Personal information

- Born: August 15, 1982 in Heraklion Crete, Greece.
- Marital Status: Married with 2 children.
- Citizenship: Greek.
- Languages: Greek and English.

### Education

- **BSc** in Applied Mathematics, Department of Applied Mathematics,
University of Crete (2000-2005). Grade: 7.55/10.0

- **MSc** in Mathematical Modeling and Scientific Computing, Departments of
Mathematics and Applied Mathematics, University of Crete (2005-2008).

  - Master thesis: On the use of Genetic Algorithms and a statistical characterization of the acoustic signal for tomographic and bottom geoacoustic inversions (in Greek).

  - Supervisor : Michael Taroudakis.

- **PhD** in Applied Mathematics, Department of Mathematics and Applied Mathematics, University of Crete (2012-2019).

	- PhD thesis : Acoustic Signal Characterization using Hidden Markov Models with applications in Acoustical Oceanography

  - Supervisor : Michael Taroudakis.

### Employment

- 2003/02-2003/06: Work Placement, Sensitivity Kernels of the Green function in Ocean Acoustic Waveguides, IACM/FORTH.

  - Supervisor : Emmanuel Skarsoulis.

- 2006-2008 and 2012-2019: Teaching Assistant at Department of Mathematics and Applied Mathematics.

- 2013/09-2015/08: Research Fellow (KRHPIS/PEFYKA - General Secretariat for Research and Technology), IACM/FORTH.

- 2016/01-2016/10: Research Fellow (SIEMENS - General Secretariat for Research and Technology), IACM/FORTH.

- 2017/04-2018/04: Research Fellow (ARCHERS - Stavros Niarchos Foundation), IACM/FORTH.

- 2018/08-2019/07: Research Fellow (KRIPIS - General Secretariat for Research and Technology), IACM/FORTH.

- 2019/08-2019/11: Postdoctoral Researcher (KRIPIS - General Secretariat for Research and Technology), IACM/FORTH.

### Research Interests

- Acoustical Oceanography
- Seismology and Ocean Seismo-Acoustics
- Probabilistic Models
- Pattern Recognition and Machine Learning
- Statistical Inference
- Information Theory

### Technical Skills

- Programming Languages: Fortran, C, **C++**, Rust, Java, **Python**, Matlab.
- Scientific Libraries: **Numpy**, Scipy, Scikit-learn, **TensorFlow**, Pandas, GSL, **ObsPy**
- Seismological Software: Seiscomp3, Instaseis, AxiSEM, Specfm2D
- Databases: MariaDB, SQLite
- Operating System: GNU/Linux (Fedora/CentOS/RHEL).
- Software Containerization Platforms: Docker-Podman, Conda, Virtualenv.
- Typesetting: LaTeX, Markdown.

### Code
- https://gitlab.com/kesmarag
- https://github.com/kesmarag

### Research Mentorships
- 2019/09 - 2019/12: Assistant Mentor, University of Crete
  - Olga Sambataro's trainship (Erasmus+ EQF level 7)
	- Underwater Acoustics, Propagation Modeling, Signal Processing

### Peer Reviews
- **Sensors** by MDPI

### Memberships
- (since 2014) Hellenic Institute of Acoustics (HELINA).
- (since 2018) Seismological Society of America (SSA).
- (since 2019) American Statistical Association (ASA) [ID 223078]
- (since 2019) Ocean Expert [ID 37775]

### Publications

- 2017: Taroudakis M., Smaragdakis C and Chapman N.R.: "De-noising procedures for inverting underwater acoustic signals in applications of acoustical oceanography" J. Comp. Acous. Vol. 25, 1750015 https://doi.org/10.1142/S0218396X17500151.
- 2014: Taroudakis M.I., Smaragdakis C. and Chapman, N.R. "Inversion of acoustical data from the `Shallow Water 06' experiment, using a statistical method for signal characterization " Journal of the Acoustical Society of America Vol. 136, pp. EL336-EL342.
- 2013: Taroudakis M.I. and Smaragdakis C. "Inversions of statistical parameters of an acoustic signal in range-dependent environments with applications in ocean acoustic tomography" Journal of the Acoustical Society of America Vol. 134, pp 2814-2822.
- 2009: Taroudakis M.I. and Smaragdakis C. "On the use of Genetic Algorithms and a statistical characterization of the acoustic signal for tomographic and bottom geoacoustic inversions Acta Acustica united with Acustica Vol 95, No 5, pp 814-822.

### Conference Proceedings
- 2019: Smaragdakis C., Taroudakis M. "Acoustic Signal Characterization using Hidden
Markov Models with applications in Acoustical Oceanography.", Abstract in Proceedings ICA 2019 and
EAA Euroregio,  9 - 13 September 2019, Aachen, Germany, pp 5399
- 2018: Smaragdakis C., Mastrokalos J. and Taroudakis M. "Classification of acoustic and seismic signals based on the statistics of their wavelet sub-band coefficients", The Journal of the Acoustical Society of America 144(3):1914-1914 DOI: 10.1121/1.5068386.
- 2018: Taroudaki V., Taroudakis M. and Smaragdakis C. "Statistical optimal filtering method for acoustical signal deblurring", The Journal of the Acoustical Society of America 144(3):1689-1689 DOI: 10.1121/1.5067509
- 2018: Smaragdakis C., Taroudakis M. "Similarity measurements of acoustical and seismic signals using Hidden Markov Models", In Proceedings of 5th Acoustic Conference of HELINA, Patras (In Greek)
- 2017: Taroudaki V., Smaragdakis C. and Taroudakis M. "Statistical Near-Optimal Filtering Method with Application to Underwater Acoustics", Abstract of Paper Procedings in AMS Meetings, Vol 38 No 1, Issue 187, p 220.
- 2016: Taroudaki V., Smaragdakis C. and Taroudakis M. "Deblurring acoustic signals for statistical characterization in application of ocean acoustic tomography" in the Journal of the Acoustical Society of America 140(4):3135-3135.
- 2016: Taroudakis M. and Smaragdakis C. "Ocean acoustic tomography using a three-phased probabilistic model-based inversion scheme" in Proceedings of the ICA 2016, Buenos Aires.
- 2016: Smaragdakis C. and Taroudakis M. "Hidden Markov Models feature extraction for inverting underwater acoustic signals using wavelet packet coefficients", EuroRegio2016, Porto, Portugal.
- 2016: Smaragdakis C., Taroudakis M. "Ocean acoustic tomography using a three-phased probabilistic model-based inversion scheme", In Proceedings of the 4th Acoustic Conference of HELINA, Piraeus (In Greek).
- 2015: Taroudakis M. and Smaragdakis C. "De-noising procedures for inverting underwater acoustic signals in applications of acoustical oceanography" in Proceedings of EuroNoise 2015 31 May - 3 June, Maastricht.
- 2014: Taroudakis M. and Smaragdakis C. "A hybrid approach for ocean acoustic tomography in range dependent environments based on statistical characterization of the acoustic signal and the identification of modal arrivals" in Proceedings of FORUM ACUSTICUM 2014 (CD edition), Krakow, Poland.
- 2014: Smaragdakis C and Taroudakis M. "Characterization of underwater acoustic signals, using a bio-mathematical model of the psycho-acoustic mechanisms of Humpback whales." in Proceedings of 7th Acoustic Conference of HELINA, Thessaloniki (In Greek).
- 2014: Taroudakis M. and Smaragdakis C. "A hybrid approach for ocean acoustic tomography based on statistical characterization of the acoustic signal and the identification of modal arrivals" in Proceedings of the 2nd Underwater Acoustics Conference edited by J.S. Papadakis and L. Bjorno, Rhodes, Greece, pp 691-698.
- 2012: Taroudakis M. and Smaragdakis C. : "Inversions of Statistical Parameters of an Acoustic Signal in Range-Dependent Environments, with Applications in Ocean Acoustic Tomography" in Proceedings of the 11th European Conference on Underwater Acoustics, Edinburgh, pp 962-969.
- 2010: Papadakis P., Smaragdakis C., Taroudakis M. and Tolstoy A.: "Hybrid inversion techniques for geoacoustic inversion" in Proceedings of the 9th European Conference on Underwater Acoustics, Istanbul.
- 2010: Taroudakis M. and Smaragdakis C. : "Underwater Acoustic Signal Characterization in the presence of Noise", in Internoise 2010, CD Rom edition, Lisbon.

### Oral presentations (without Proceedings)
- 2017: Smaragdakis C. and Taroudakis M. : "A probabilistic approach based on Hidden Markov Models for the estimation of the geoacoustic parameters of the sea bottom", 4th Underwater Acoustics Conference and Exhibition, Skiathos, Greece.

